/**
 * 通用js方法封装处理
 */

//  添加日期范围
// params 中间参数
export function addDateRange(params,dateRange){
    var search = params;
    search.beginTime = "";
    search.endTime = "";
    if(null != dateRange && '' != dateRange){
        search.beginTime = this.dateRange[0];
        search.endTime = this.dateRange[1];
    }
    return search;
}

// 表单重置
// resetFields 对整个表单进行重置，将所有字段值重置为空并移除校验结果
export function resetForm(refName) {
	if (this.$refs[refName]) {
		this.$refs[refName].resetFields();
	}
}

// 通用下载方法
export function download(fileName) {
	window.location.href = baseURL + "/common/download?fileName=" + encodeURI(fileName) + "&delete=" + true;
}
