// 用户相关操作方法

// 查询用户列表 < 按一定条件（参数）查找用户 >
export function listUser(query){
    return request({
        url: '/system/user/list',
        method: 'get',
        params: query
    })
}
